
Configuration reader with league/container service provider

# Installation

## yml extension

    pecl install yml

## composer

    composer require slashlead/configuration

# Configuration files
The path where configuration is stored and the environment name must be exposed in the $_SERVER.

the key names are:
- application_mode, the name of the yml file that is loaded
- config_dir, the dir where configuration files are stored


Configuration files need to be stored in yml format

# How to use

## Provided service
the parsed config array will be provided in the container with alias "configStore"

## Example code

    <?php
    /*
     For demo use we set $_SERVER parameters in this script
     with this example code the following config will be loaded
     /var/www/html/config/development.yml
     */
    
    $_SERVER['application_mode'] = 'development';
    $_SERVER['config_dir']       = '/var/www/html/config';
    
    $container = new \League\Container\Container();
    $container->addServiceProvider(new Slashlead\Component\Configuration\ServiceProvider);
   
    print_r($container->get('configStore'));