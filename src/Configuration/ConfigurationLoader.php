<?php
namespace Slashlead\Component\Configuration;

/**
 * @author Samuel Carlier
 */
class ConfigurationLoader
{
    const ENV_PARAMETER_MODE = 'application_mode';
    const ENV_PARAMETER_DIR = 'config_dir';

    /**
     * return lowercase environment name
     *
     * @return string
     * @throws \Exception
     */
    private function getEnvironmentName() : string
    {
        if (!array_key_exists(self::ENV_PARAMETER_MODE, $_SERVER)) {
            throw new \Exception('Environment variable "' . self::ENV_PARAMETER_MODE . '" not found');
        }

        return strtolower(trim($_SERVER[self::ENV_PARAMETER_MODE]));
    }

    /**
     * return environment config dir
     *
     * @return string
     * @throws \Exception
     */
    private function getEnvironmentConfigDir() : string
    {
        if (!array_key_exists(self::ENV_PARAMETER_DIR, $_SERVER)) {
            throw new \Exception('Environment variable "' . self::ENV_PARAMETER_DIR . '" not found');
        }

        return rtrim($_SERVER[self::ENV_PARAMETER_DIR], '/');
    }

    /**
     * return path to Environment config file
     *
     * @param string $environmentName
     * @return string
     * @throws \Exception
     */
    private function getEnvironmentConfig(string $environmentName) : string
    {
        $path = $this->getEnvironmentConfigDir() . '/' . $environmentName . '.yml';
        if (!file_exists($path)) {
            throw new \Exception('Configuration "' . $this->getEnvironmentName() . '.yml' . '" 
                not found in directory "' . $this->getEnvironmentConfigDir() . '"');
        }

        return $path;
    }

    /**
     * @param string $environmentName if not set it will try to find it in a environment variable
     * @return array
     * @throws \Exception
     */
    public function getConfig($environmentName = null) : array
    {
        if (is_null($environmentName)) {
            $environmentName = $this->getEnvironmentName();
        }

        $config = yaml_parse_file($this->getEnvironmentConfig($environmentName));
        if (!$config) {
            throw new \Exception('Configuration "' . $this->getEnvironmentName() . '.yml' . '" did not parse');
        }

        return $config;
    }
}