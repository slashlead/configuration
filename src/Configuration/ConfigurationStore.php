<?php
namespace Slashlead\Component\Configuration;

/**
 * @author Samuel Carlier
 */
class ConfigurationStore implements \ArrayAccess
{
    /**
     * @var array
     */
    private $container = [];

    /**
     *
     */
    public function __construct()
    {
        $this->genConfig();
    }

    private function genConfig()
    {
        $configLoader = new ConfigurationLoader();
        $this->container = $configLoader->getConfig();
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

}