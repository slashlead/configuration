<?php
namespace Slashlead\Component\Configuration;

use League\Container\ServiceProvider\AbstractServiceProvider;

/**
 * @author Samuel Carlier
 */
class ServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
        ConfigurationStore::class
    ];

    /**
     * Use the register method to register items with the container via the
     * protected $this->container property or the `getContainer` method
     * from the ContainerAwareTrait.
     *
     * @return void
     */
    public function register()
    {
        $this->container->share(ConfigurationStore::class);
    }
}